var db = require("../model/index"); 
const category = db.category; 

var catRead= async(req,res)=>{
    let data= await category.findAll({
    }); 
    console.log(data.dataValues);
    let response={
        data:data
    }
    res.status(200).json(response);
}
var catInsert = async (req, res) => {
    let { name ,type} = req.body;
    let data = await category.create({ name: name,type:type }) 
    console.log(data.dataValues);
    let response = {
        data: "insert"
    }
    res.status(200).json(response);
}

var catUpdate = async (req, res) => {
    let { name, type } = req.body;
    let { id } = req.params;

    let data = await category.update({ name: name,type:type }, {
        where: {
            id: id
        }
    });
    let response = {
        data: "update"
    }
    res.status(200).json(response);
}

var catDelete = async (req, res) => {
    let { id } = req.params;
    let data = await category.destroy({
        where: {
            id: id
        }
    }); 
    console.log(data.dataValues);
    let response = {
        data: "delete"
    }
    res.status(200).json(response);
}
module.exports = {
    catInsert,
    catUpdate,
     catDelete,
      catRead
}
