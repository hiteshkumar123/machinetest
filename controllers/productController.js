var db = require("../model/index"); 

const product = db.product; 

var prodRead= async(req,res)=>{
    let limit=(req.query.limit)?parseInt(req.query.limit):10
    let offset=(req.query.offset)?parseInt(req.query.offset):1
    let data= await product.findAll({
        include: [
            {   
                model:db.category,
                as: 'categories',
            }
        ],
        limit:limit,
        offset:offset
    });
    let count=await product.count({})
    let response={
        data:data,
        count:count
    }
    res.status(200).json(response);
}

var prodInsert = async (req, res) => {
    let { name ,type ,categoryId} = req.body;
    let data = await product.create({ name: name ,type:type,categoryId:categoryId}) 
    console.log(data.dataValues);
    let response = {
        data: "insert"
    }
    res.status(200).json(response);
}

var prodUpdate = async (req, res) => {
    let { name,type } = req.body;
    let { id } = req.params;

    let data = await product.update({ name: name,type:type }, {
        where: {
            id: id
        }
    });
    let response = {
        data: "update"
    }
    res.status(200).json(response);
}

var prodDelete = async (req, res) => {
    let { id } = req.params;
    let data = await product.destroy({
        where: {
            id: id
        }
    }); 
    let response = {
        data: "delete"
    }
    res.status(200).json(response);
}
module.exports = {
    prodRead,
    prodInsert,
    prodUpdate,
    prodDelete
}
