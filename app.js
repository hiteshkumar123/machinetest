const express =require('express');
const bodyParser = require("body-parser");
const path = require('path'); 
const db = require('./model');
var productCtrl=require('./controllers/productController'); 
var categoryCtrl=require('./controllers/categoryController');
const PORT = process.env.PORT || 5000;
const app = express();
app.use(express.json())
app.get('/',(req,res)=>res.status(200).send("welcome to home page"))
app.get('/read/product',productCtrl.prodRead)
app.post('/insert/product',productCtrl.prodInsert)  
app.put('/update/product/:id',productCtrl.prodUpdate)  
app.delete('/delete/product/:id',productCtrl.prodDelete)
app.get('/read/category',categoryCtrl.catRead)
app.post('/insert/category',categoryCtrl.catInsert) 
app.put('/update/category/:id',categoryCtrl.catUpdate)  
app.delete('/delete/category/:id',categoryCtrl.catDelete) 
app.listen(PORT,console.log(`server started on port ${PORT}`));