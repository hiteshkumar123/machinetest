module.exports = (sequelize, DataTypes) => { 
    const category = sequelize.define("category", { 
        name: {
            type: DataTypes.STRING
        },
        type:{
            type:DataTypes.STRING
        }
    });
    return category;
} 
