module.exports = (sequelize, DataTypes) => { //??
    const product = sequelize.define('product', { //??
        name: {
            type: DataTypes.STRING
        },
        type:{
            type:DataTypes.STRING
        },
        categoryId:{
            type:DataTypes.INTEGER
        }
    });
    product.associate = (model) =>{
        product.belongsTo(model.category,{foreignKey:'categoryId' , as:'categories'})
       

    }
    return product;
} 
